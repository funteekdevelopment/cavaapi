﻿using Cava.Contracts.ViewModels.Post;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.BusinessLogic.Interfaces
{
    /// <summary>
    /// The IPostManager
    /// </summary>
    public interface IPostManager
    {
        /// <summary>
        /// Get all posts
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<PostMobileResponse> Posts(long userId);
    }
}
