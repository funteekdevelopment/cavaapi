﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.BusinessLogic.Interfaces
{
    /// <summary>
    /// The ISmsManager
    /// </summary>
    public interface ISmsManager
    {
        /// <summary>
        /// The SendSms
        /// </summary>
        /// <param name="mobilePhone"></param>
        void SendSms(string mobilePhone);
    }
}
