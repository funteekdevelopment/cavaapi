﻿using Cava.Contracts.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.BusinessLogic.Interfaces
{
    /// <summary>
    /// The IUserManager
    /// </summary>
    public interface IUserManager
    {
        /// <summary>
        /// The Login
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        UserLoginResponse Login(UserLoginRequest request);

        /// <summary>
        /// The Registration
        /// </summary>
        /// <param name=""></param>
        void Registration(UserRegisterRequest request);
    }
}
