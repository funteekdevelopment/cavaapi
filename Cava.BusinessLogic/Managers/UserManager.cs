﻿using Cava.BusinessLogic.Interfaces;
using Cava.Common.Security;
using Cava.Contracts.ViewModels.User;
using Cava.DataAccess.Interfaces;
using Cava.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.BusinessLogic.Managers
{
    /// <summary>
    /// The UserManager
    /// </summary>
    public class UserManager : IUserManager
    {
        #region Private Fields

        /// <summary>
        /// The _userRepository
        /// </summary>
        private IUserRepository _userRepository;

        /// <summary>
        /// The _securityManager
        /// </summary>
        private ISecurityManager _securityManager;

        /// <summary>
        /// The _smsManager
        /// </summary>
        private ISmsManager _smsManager;

        #endregion

        #region Private Property

        /// <summary>
        /// The securityManager
        /// </summary>
        private ISecurityManager securityManager
        {
            get
            {
                if (_securityManager == null) _securityManager = new SecurityManager();

                return _securityManager;
            }
        }

        /// <summary>
        /// The userRepository
        /// </summary>
        private IUserRepository userRepository
        {
            get
            {
                if (_userRepository == null) _userRepository = new UserRepository();

                return _userRepository;
            }
        }

        /// <summary>
        /// The userRepository
        /// </summary>
        private ISmsManager smsManager
        {
            get
            {
                if (_smsManager == null) _smsManager = new SmsManager();

                return _smsManager;
            }
        }

        #endregion

        #region

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManager" /> class.
        /// </summary>
        public UserManager() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManager" /> class.
        /// </summary>
        /// <param name="userReopsitory"></param>
        /// <param name="securityManager"></param>
        public UserManager(IUserRepository userReopsitory, ISecurityManager securityManager)
        {
            this._userRepository = userReopsitory;
            this._securityManager = securityManager;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManager" /> class.
        /// </summary>
        /// <param name="userReopsitory"></param>
        /// <param name="securityManager"></param>
        /// <param name="smsManager"></param>
        public UserManager(IUserRepository userReopsitory, ISecurityManager securityManager, ISmsManager smsManager)
        {
            this._userRepository = userReopsitory;
            this._securityManager = securityManager;
            this._smsManager = smsManager;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// The login user by number of phone and password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public UserLoginResponse Login(UserLoginRequest request)
        {
            var user = userRepository.Login(request.mobile_phone, request.password);

            if (user == null) throw new ArgumentException("User with this credentials not found");

            return new UserLoginResponse
            {
                token = securityManager.BuildToken(user.Id)
            };
        }

        /// <summary>
        /// The Registration
        /// </summary>
        /// <param name="request"></param>
        public void Registration(UserRegisterRequest request)
        {
            var user = userRepository.UserByPhone(request.mobile_phone);

            if (user != null) throw new ArgumentException("User with this mobile number alredy exist");

            var password = securityManager.GeneratePassword();

            userRepository.Create(request.mobile_phone, password);

            try
            {
                smsManager.SendSms(request.mobile_phone);
            }
            catch(Exception)
            {
                userRepository.DeleteByPhone(request.mobile_phone);

                throw;
            }
        }

        #endregion
    }
}
