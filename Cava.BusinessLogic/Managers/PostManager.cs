﻿using Cava.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cava.Contracts.ViewModels.Post;
using Cava.DataAccess.Repositories;
using Cava.DataAccess.Interfaces;
using Cava.DataAccess.Views;

namespace Cava.BusinessLogic.Managers
{
    /// <summary>
    /// The PostManager
    /// </summary>
    public class PostManager : IPostManager
    {
        #region Fields

        /// <summary>
        /// The _postRepository
        /// </summary>
        private IPostRepository _postRepository;

        #endregion

        #region Private Property

        /// <summary>
        /// The postRepository
        /// </summary>
        private IPostRepository postRepository
        {
            get
            {
                if (this._postRepository == null) this._postRepository = new PostRepository();

                return this._postRepository;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initialize PostManager
        /// </summary>
        public PostManager() { }

        /// <summary>
        /// Initialize PostManager
        /// </summary>
        /// <param name="postRepository"></param>
        public PostManager(IPostRepository postRepository)
        {
            this._postRepository = postRepository;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// The Posts
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<PostMobileResponse> Posts(long userId)
        {
            var result = new List<PostMobileResponse>();

            List<PostMobile> posts = postRepository.Posts(userId);

            if (posts != null)
            {
                result.AddRange(posts.Where(item => item.MyLikes > 0).Select(item => new PostMobileResponse
                {
                    likes_count = item.LikesCount,
                    id = item.PostId,
                    image = item.Image,
                    title = item.Title,
                    create_date = item.CreateDate,
                    is_like = true
                }));

                result.AddRange(posts.Where(item => item.MyLikes == 0).Select(item => new PostMobileResponse
                {
                    likes_count = item.LikesCount,
                    id = item.PostId,
                    image = item.Image,
                    title = item.Title,
                    create_date = item.CreateDate,
                    is_like = false
                }));
            }

            return result.OrderByDescending(item => item.create_date).ToList();
        }

        #endregion
    }
}
