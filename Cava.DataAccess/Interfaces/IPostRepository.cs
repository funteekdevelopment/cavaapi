﻿using Cava.DataAccess.Models;
using Cava.DataAccess.Repositories;
using Cava.DataAccess.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.DataAccess.Interfaces
{
    /// <summary>
    /// The IPostRepository
    /// </summary>
    public interface IPostRepository
    {
        /// <summary>
        /// Get all posts
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<PostMobile> Posts(long userId);
    }
}
