﻿using Cava.Contracts.ViewModels.User;
using Cava.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.DataAccess.Interfaces
{
    /// <summary>
    /// The IUserRepository
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// The Login
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        User Login(string mobilePhone, string password);

        /// <summary>
        /// The UserByPhone
        /// </summary>
        /// <param name="mobilePhone"></param>
        /// <returns></returns>
        User UserByPhone(string mobilePhone);

        /// <summary>
        /// The Create
        /// </summary>
        /// <param name="mobilePhone"></param>
        void Create(string mobilePhone, string password);

        /// <summary>
        /// The DeleteByPhone
        /// </summary>
        /// <param name="mobilePhone"></param>
        void DeleteByPhone(string mobilePhone);
    }
}
