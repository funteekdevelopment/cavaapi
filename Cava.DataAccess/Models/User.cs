﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.DataAccess.Models
{
    /// <summary>
    /// The User
    /// </summary>
    public class User : EntryBase
    {
        #region Property

        /// <summary>
        /// MobilePhone
        /// </summary>
        [Required]
        public string MobilePhone { get; set; }

        /// <summary>
        /// The PointsCount
        /// </summary>
        [Required]
        public int PointsCount { get; set; }

        /// <summary>
        /// The Password
        /// </summary>
        [Required]
        public string Password { get; set; }
        
        #endregion
    }
}
