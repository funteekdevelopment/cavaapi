﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.DataAccess.Models
{
    /// <summary>
    /// The UserLikes
    /// </summary>
    public class UserLike : EntryBase
    {
        /// <summary>
        /// The UserId
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// The PostId
        /// </summary>
        public long PostId { get; set; }
    }
}
