﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.DataAccess.Models
{
    /// <summary>
    /// The Post
    /// </summary>
    public class Post : EntryBase
    {
        #region Property

        /// <summary>
        /// The Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The Image
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// The LikesCount
        /// </summary>
        public int LikesCount { get; set; }

        #endregion
    }
}
