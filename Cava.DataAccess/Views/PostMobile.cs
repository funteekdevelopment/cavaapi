﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.DataAccess.Views
{
    /// <summary>
    /// The PostMobile
    /// </summary>
    public class PostMobile
    {
        /// <summary>
        /// The PostId
        /// </summary>
        public long PostId { get; set; }

        /// <summary>
        /// The Image
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// The LikesCount
        /// </summary>
        public int LikesCount { get; set; }

        /// <summary>
        /// The Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The MyLikes
        /// </summary>
        public int MyLikes { get; set; }

        /// <summary>
        /// The CreateDate
        /// </summary>
        public DateTime CreateDate { get; set; }
    }
}
