namespace Cava.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2306171047 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tUser", "MobilePhone", c => c.String(nullable: false));
            AlterColumn("dbo.tUser", "Password", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tUser", "Password", c => c.String());
            AlterColumn("dbo.tUser", "MobilePhone", c => c.String());
        }
    }
}
