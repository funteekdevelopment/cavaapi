namespace Cava.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _26060856 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tUserLike",
                c => new
                    {
                        UserLikeId = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        PostId = c.Long(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserLikeId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.tUserLike");
        }
    }
}
