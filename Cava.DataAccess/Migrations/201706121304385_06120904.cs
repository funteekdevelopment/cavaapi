namespace Cava.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06120904 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tUser", "PointsCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.tUser", "PointsCount");
        }
    }
}
