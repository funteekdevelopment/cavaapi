namespace Cava.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06291151 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tPost", "LikesCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tPost", "LikesCount", c => c.String());
        }
    }
}
