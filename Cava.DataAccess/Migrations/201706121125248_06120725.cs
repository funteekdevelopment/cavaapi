namespace Cava.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06120725 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tPost",
                c => new
                    {
                        PostId = c.Long(nullable: false, identity: true),
                        Title = c.String(),
                        Image = c.String(),
                        LikesCount = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PostId);
            
            CreateTable(
                "dbo.tUser",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        MobilePhone = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.tUser");
            DropTable("dbo.tPost");
        }
    }
}
