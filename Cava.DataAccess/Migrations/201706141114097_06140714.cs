namespace Cava.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _06140714 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tUser", "Password", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.tUser", "Password");
        }
    }
}
