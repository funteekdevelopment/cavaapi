﻿using Cava.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.DataAccess.Context
{
    /// <summary>
    /// The CavaDbContext
    /// </summary>
    public class CavaDbContext : DbContext
    {
        # region Property

        /// <summary>
        /// The Users
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// The Posts
        /// </summary>
        public DbSet<Post> Posts { get; set; }

        /// <summary>
        /// The UserLikes
        /// </summary>
        public DbSet<UserLike> UserLikes { get; set; }


        #endregion

        private const string PostMpbileLoadList =
            @"SELECT 
                p.postId, 
                p.image, 
                p.likesCount, 
                p.title, 
                (SELECT count(*) FROM tUserLike ul WHERE ul.PostId = p.PostId and ul.UserId = 3) AS countLikes 
            FROM tPost as p";

        #region Constructors

        /// <summary>
        /// Initializing FunteekDbContext
        /// </summary>
        public CavaDbContext() : base("CavaDbContext")
        {

        }

        #endregion
        
        #region Protected Methods 

        /// <summary>
        /// Creating tables in db
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            InitializeEntity<User>(modelBuilder, "tUser", "UserId");
            InitializeEntity<Post>(modelBuilder, "tPost", "PostId");
            InitializeEntity<UserLike>(modelBuilder, "tUserLike", "UserLikeId");
        }

        #endregion

        #region Private Methods 

        /// <summary>
        /// Initializing tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="modelBuilder"></param>
        /// <param name="tableName"></param>
        /// <param name="dataIDFieldName"></param>
        private void InitializeEntity<T>(DbModelBuilder modelBuilder, string tableName, string dataIDFieldName) where T : EntryBase
        {
            modelBuilder.Entity<T>().ToTable(tableName);
            modelBuilder.Entity<T>().HasKey(d => d.Id);
            modelBuilder.Entity<T>().Property(d => d.Id).HasColumnName(dataIDFieldName);
        }

        /// <summary>
        /// Add new entity in table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public void AddEntity<T>(T entity) where T : EntryBase
        {
            entity.CreateDate = DateTime.UtcNow;
            Set<T>().Add(entity);
        }

        public string ReadQueryFile(string filename)
        {
            // Read query from file
            //var cteQuery = PostMpbileLoadList;
            //var path = "~/CavaApi/Cava.DataAccess/SQLFiles/Post/" + filename;
            string query;
            using (var sr = new StreamReader(PostMpbileLoadList))
            {
                query = sr.ReadToEnd();
            }

            return query;
        }

        private Stream GetEmbeddedResourceStream(string fileName)
        {
            var assembly = typeof(CavaDbContext).Assembly;
            var erName = assembly.GetManifestResourceNames().FirstOrDefault(name => name.EndsWith(fileName));
            var erStream = assembly.GetManifestResourceStream(erName);
            return erStream;
        }

        #endregion
    }
}
