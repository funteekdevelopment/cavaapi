﻿using Cava.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cava.DataAccess.Models;
using Cava.DataAccess.Context;
using System.IO;
using System.Data.SqlClient;
using Cava.DataAccess.Views;

namespace Cava.DataAccess.Repositories
{
    /// <summary>
    /// The PostRepository
    /// </summary>
    public class PostRepository : IPostRepository
    {
        #region Fields

        /// <summary>
        /// Query for getting all posts
        /// </summary>
        private const string PostMpbileLoadList =
            @"SELECT 
                p.PostId,
                p.Image, 
                p.LikesCount, 
                p.Title, 
                (SELECT count(*) FROM tUserLike ul WHERE ul.PostId = p.PostId and ul.UserId = @UserId) AS MyLikes,
                p.CreateDate 
            FROM tPost as p";

        #endregion

        #region Public Methods

        /// <summary>
        /// Get all posts
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<PostMobile> Posts(long userId)
        {
            using (var db = new CavaDbContext())
            {
                var param = new SqlParameter("@UserId", userId);

                var result = db.Database.SqlQuery<PostMobile>(PostMpbileLoadList, param).ToList();

                return result;
            }
        }

        #endregion
    }
}
