﻿using Cava.Contracts.ViewModels.User;
using Cava.DataAccess.Context;
using Cava.DataAccess.Interfaces;
using Cava.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.DataAccess.Repositories
{
    /// <summary>
    /// The UserRepository
    /// </summary>
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// The Login
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public User Login(string mobilePhone, string password)
        {
            using (var db = new CavaDbContext())
            {
                var user = db.Users.FirstOrDefault(item => item.MobilePhone == mobilePhone && item.Password == password && item.IsDeleted == false);

                return user;
            }
        }

        /// <summary>
        /// The UserByPhone
        /// </summary>
        /// <param name="mobilePhone"></param>
        /// <returns></returns>
        public User UserByPhone(string mobilePhone)
        {
            using (var db = new CavaDbContext())
            {
                var user = db.Users.FirstOrDefault(item => item.MobilePhone == mobilePhone && item.IsDeleted == false);

                return user;
            }
        }

        /// <summary>
        /// The Create
        /// </summary>
        /// <param name="mobilePhone"></param>
        public void Create(string mobilePhone, string password)
        {
            using (var db = new CavaDbContext())
            {
                var user = new User
                {
                    MobilePhone = mobilePhone,
                    Password = password,
                    PointsCount = 0
                };

                db.AddEntity(user);

                db.SaveChanges();
            }
        }

        /// <summary>
        /// The DeleteByPhone
        /// </summary>
        /// <param name="mobilePhone"></param>
        public void DeleteByPhone(string mobilePhone)
        {
            using (var db = new CavaDbContext())
            {
                var user = db.Users.FirstOrDefault(item => item.MobilePhone == mobilePhone && item.IsDeleted == false);

                user.IsDeleted = true;

                db.SaveChanges();
            }
        }
    }
}
