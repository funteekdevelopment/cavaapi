﻿using Cava.BusinessLogic.Interfaces;
using Cava.BusinessLogic.Managers;
using Cava.Common.Filters;
using Cava.Common.Security;
using Cava.Contracts.ViewModels.Post;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CavaApi.Controllers
{
    /// <summary>
    /// The PostController
    /// </summary>
    public class PostController : ApiController
    {
        private IPostManager _postManager;

        private IPostManager postManager
        {
            get
            {
                if (this._postManager == null) this._postManager = new PostManager();

                return this._postManager;
            }
        }

        /// <summary>
        /// The Posts
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [BasicAuthenticationFilter]
        [Authorize]
        public List<PostMobileResponse> Posts()
        {
            return postManager.Posts(SecurityContext.UserId);
        }
    }
}
