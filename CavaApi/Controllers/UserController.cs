﻿using Cava.BusinessLogic.Interfaces;
using Cava.BusinessLogic.Managers;
using Cava.Contracts.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CavaApi.Controllers
{
    /// <summary>
    /// The UserController
    /// </summary>
    public class UserController : ApiController
    {
        #region Private Fields

        /// <summary>
        /// The _userManager
        /// </summary>
        private IUserManager _userManager;

        #endregion

        #region Private Property

        /// <summary>
        /// The userManager
        /// </summary>
        private IUserManager userManager
        {
            get
            {
                if (_userManager == null) _userManager = new UserManager();

                return _userManager;
            }
        }

        #endregion

        #region Actions

        /// <summary>
        /// The Login
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public UserLoginResponse Login(UserLoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState));
            }
            try
            {
                return this.userManager.Login(request);
            }
            catch (ArgumentException ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message));
            }
            catch (Exception)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An error has occurred"));
            }
        }

        [HttpPost]
        public HttpResponseMessage Registration(UserRegisterRequest request)
        {
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState));
            }

            try
            {
                this.userManager.Registration(request);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ArgumentException ex)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message));
            }
            catch (Exception)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "An error has occurred"));
            }
        }

        #endregion
    }
}
