﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CavaApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            var i = 0;

            config.Routes.MapHttpRoute(name: "Entity" + (i++), routeTemplate: "user/login", defaults: new { controller = "User", action = "Login" });
            config.Routes.MapHttpRoute(name: "Entity" + (i++), routeTemplate: "user/register", defaults: new { controller = "User", action = "Registration" });

            config.Routes.MapHttpRoute(name: "Post" + (i++), routeTemplate: "post/all", defaults: new { controller = "Post", action = "Posts" });


            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}
