﻿using Cava.BusinessLogic.Interfaces;
using Cava.BusinessLogic.Managers;
using Cava.Common.Security;
using Cava.Contracts.ViewModels.User;
using Cava.DataAccess.Interfaces;
using Cava.DataAccess.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.BusinessLogic.Tests.Managers
{
    /// <summary>
    /// The UserManagerTest
    /// </summary>
    [TestClass]
    public class UserManagerTest
    {
        #region Fields

        /// <summary>
        /// The userManager
        /// </summary>
        private IUserManager userManager;

        /// <summary>
        /// The userRepositoryMock
        /// </summary>
        private Mock<IUserRepository> userRepositoryMock;

        /// <summary>
        /// The securityManagerMock
        /// </summary>
        private Mock<ISecurityManager> securityManagerMock;

        /// <summary>
        /// The smsManager
        /// </summary>
        private Mock<ISmsManager> smsManager;

        #endregion

        #region Public Test Methods

        /// <summary>
        /// Successfully test for login request
        /// </summary>
        [TestMethod]
        public void Login_Successfully_Test()
        {
            /// Arrange
            string mobile_phone = "+380679460986";
            string password = "test";
            string token = "ASFDAFD994523ASD=";
            UserLoginRequest request = new UserLoginRequest { mobile_phone = mobile_phone, password = password };
            User user = new User { CreateDate = DateTime.Now.AddDays(-5), Id = 1, IsDeleted = false, MobilePhone = mobile_phone, Password = password, PointsCount = 10 };
            this.userRepositoryMock = new Mock<IUserRepository>();
            this.userRepositoryMock.Setup(item => item.Login(It.IsAny<string>(), It.IsAny<string>())).Returns(user);
            this.securityManagerMock = new Mock<ISecurityManager>();
            this.securityManagerMock.Setup(item => item.BuildToken(It.IsAny<long>())).Returns(token);
            this.userManager = new UserManager(this.userRepositoryMock.Object, this.securityManagerMock.Object);

            /// Act
            UserLoginResponse result = this.userManager.Login(request);

            /// Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(token, result.token);
        }

        /// <summary>
        /// Not successfully test for login request
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Login_Not_Successfully_Test()
        {
            /// Arrange
            string mobile_phone = "+380679460986";
            string password = "test";
            UserLoginRequest request = new UserLoginRequest { mobile_phone = mobile_phone, password = password };
            User user = null;
            this.userRepositoryMock = new Mock<IUserRepository>();
            this.userRepositoryMock.Setup(item => item.Login(It.IsAny<string>(), It.IsAny<string>())).Returns(user);
            this.securityManagerMock = new Mock<ISecurityManager>();
            this.userManager = new UserManager(this.userRepositoryMock.Object, this.securityManagerMock.Object);

            /// Act
            UserLoginResponse result = this.userManager.Login(request);
        }

        /// <summary>
        /// Successfully test for registration
        /// </summary>
        [TestMethod]
        public void Registration_Successfully_Test()
        {
            // Arrange
            string mobile_phone = "+380679460986";
            string password = "test";
            UserRegisterRequest request = new UserRegisterRequest { mobile_phone = mobile_phone };
            User user = null;
            this.userRepositoryMock = new Mock<IUserRepository>();
            this.userRepositoryMock.Setup(item => item.UserByPhone(It.IsAny<string>())).Returns(user);
            this.securityManagerMock = new Mock<ISecurityManager>();
            this.securityManagerMock.Setup(item => item.GeneratePassword()).Returns(password);
            this.userRepositoryMock = new Mock<IUserRepository>();
            this.smsManager = new Mock<ISmsManager>();
            this.userManager = new UserManager(this.userRepositoryMock.Object, this.securityManagerMock.Object, this.smsManager.Object);

            // Act
            this.userManager.Registration(request);

            // Assert
            this.userRepositoryMock.Verify(item => item.Create(It.IsAny<string>(), It.IsAny<string>()));
            this.smsManager.Verify(item => item.SendSms(It.IsAny<string>()));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Registration_Not_Successfully_Test()
        {
            // Arrange
            string mobile_phone = "+380679460986";
            string password = "test";
            UserRegisterRequest request = new UserRegisterRequest { mobile_phone = mobile_phone };
            User user = new User { CreateDate = DateTime.Now.AddDays(-5), Id = 1, IsDeleted = false, MobilePhone = mobile_phone, Password = password, PointsCount = 10 };
            this.userRepositoryMock = new Mock<IUserRepository>();
            this.userRepositoryMock.Setup(item => item.UserByPhone(It.IsAny<string>())).Returns(user);
            this.securityManagerMock = new Mock<ISecurityManager>();
            this.smsManager = new Mock<ISmsManager>();
            this.userManager = new UserManager(this.userRepositoryMock.Object, this.securityManagerMock.Object, this.smsManager.Object);

            // Act
            this.userManager.Registration(request);
        }

        #endregion
    }
}
