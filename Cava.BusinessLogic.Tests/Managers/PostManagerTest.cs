﻿using Cava.BusinessLogic.Interfaces;
using Cava.BusinessLogic.Managers;
using Cava.Contracts.ViewModels.Post;
using Cava.DataAccess.Interfaces;
using Cava.DataAccess.Views;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.BusinessLogic.Tests.Managers
{
    /// <summary>
    /// The PostManagerTest
    /// </summary>
    [TestClass]
    public class PostManagerTest
    {
        #region Fields

        /// <summary>
        /// The postManager
        /// </summary>
        private IPostManager postManager;

        /// <summary>
        /// The userRepositoryMock
        /// </summary>
        private Mock<IPostRepository> userRepositoryMock;

        #endregion

        #region Public Methods

        [TestMethod]
        public void Post_With_Value_Test()
        {
            // Arrange
            var userId = 1;
            var post1 = new PostMobile
            {
                PostId = 1,
                CreateDate = DateTime.Now,
                Image = "http://image1",
                LikesCount = 10,
                Title = "test1",
                MyLikes = 0
            };

            var post2 = new PostMobile
            {
                PostId = 2,
                CreateDate = DateTime.Now.AddMinutes(-1),
                Image = "http://image2",
                LikesCount = 20,
                Title = "test1",
                MyLikes = 2
            };

            var response = new List<PostMobile>();
            response.Add(post1);
            response.Add(post2);
            this.userRepositoryMock = new Mock<IPostRepository>();
            this.userRepositoryMock.Setup(item => item.Posts(It.IsAny<long>())).Returns(response);
            this.postManager = new PostManager(this.userRepositoryMock.Object);

            // Act
            List<PostMobileResponse> result = postManager.Posts(userId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result[0].is_like, false);
            Assert.AreEqual(result[1].is_like, true);
        }

        [TestMethod]
        public void Post_Without_Value_Test()
        {
            // Arrange
            var userId = 1;
            List<PostMobile> response = null;
            this.userRepositoryMock = new Mock<IPostRepository>();
            this.userRepositoryMock.Setup(item => item.Posts(It.IsAny<long>())).Returns(response);
            this.postManager = new PostManager(this.userRepositoryMock.Object);

            // Act
            List<PostMobileResponse> result = postManager.Posts(userId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 0);
        }

        #endregion
    }
}
