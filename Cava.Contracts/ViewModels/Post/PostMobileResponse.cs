﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.Contracts.ViewModels.Post
{
    /// <summary>
    /// The PostMobileResponse
    /// </summary>
    public class PostMobileResponse
    {
        /// <summary>
        /// The id
        /// </summary>
        public long id { get; set; }

        /// <summary>
        /// The title
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// The image
        /// </summary>
        public string image { get; set; }

        /// <summary>
        /// The count_likes
        /// </summary>
        public int likes_count { get; set; }

        /// <summary>
        /// The is_like
        /// </summary>
        public bool is_like { get; set; }

        /// <summary>
        /// The create_date
        /// </summary>
        public DateTime create_date { get; set; }
    }
}
