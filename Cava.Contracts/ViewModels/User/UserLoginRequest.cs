﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.Contracts.ViewModels.User
{
    /// <summary>
    /// The UserLoginRequestv
    /// </summary>
    public class UserLoginRequest
    {
        /// <summary>
        /// The mobile_phone
        /// </summary>
        [Required(ErrorMessage = "mobile_phone is empty")]
        public string mobile_phone { get; set; }

        /// <summary>
        /// The password
        /// </summary>
        [Required(ErrorMessage = "password is empty")]
        public string password { get; set; }
    }
}
