﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.Contracts.ViewModels.User
{
    /// <summary>
    /// The UserLoginResponse
    /// </summary>
    public class UserLoginResponse
    {
        /// <summary>
        /// The token
        /// </summary>
        public string token { get; set; }
    }
}
