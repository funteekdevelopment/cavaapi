﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Cava.Common.Security
{
    /// <summary>
    /// The RequestIdentity
    /// </summary>
    public class RequestIdentity : IIdentity
    {
        /// <summary>
        /// The Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The AuthenticationType
        /// </summary>
        public string AuthenticationType { get { return "Token"; } }

        /// <summary>
        /// The IsAuthenticated
        /// </summary>
        public bool IsAuthenticated { get { return true; } }
    }
}
