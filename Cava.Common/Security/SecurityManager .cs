﻿using Cava.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Cava.Common.Security
{
    /// <summary>
    /// The SecurityManager
    /// </summary>
    public class SecurityManager : ISecurityManager
    {
        /// <summary>
        /// Parsing token 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public IPrincipal ParseToken(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                return null;
            }

            var bytes = Convert.FromBase64String(token);

            var data = Encoding.UTF8.GetString(bytes);

            var parts = data.Split('|');

            if (parts.Length != 2)
            {
                return null;
            }

            long? userId = parts[0].AsLong();

            DateTime? date = parts[1].AsDateTime();

            if (userId == null || date == null)
            {
                return null;
            }

            var identity = new RequestIdentity();

            return new RequestPrincipal(identity, userId.Value, date.Value);
        }

        /// <summary>
        /// Creating token
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string BuildToken(long? userID)
        {
            string data = userID + "|" + DateTime.Now;

            var bytes = Encoding.UTF8.GetBytes(data);

            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// Generating password, length 4
        /// </summary>
        /// <returns></returns>
        public string GeneratePassword()
        {
            var random = new Random();

            return random.Next(1000, 9999).ToString();
        }
    }
}
