﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cava.Common.Security
{
    public static class SecurityContext
    {
        public static long UserId
        {
            get { return ((RequestPrincipal)HttpContext.Current.User).UserId ?? 0L; }
        }
        
        public static bool IsUserAuthenticated
        {
            get { return UserId != 0L; }
        }

        public static DateTime Date
        {
            get { return ((RequestPrincipal)HttpContext.Current.User).Date; }
        }
    }
}
