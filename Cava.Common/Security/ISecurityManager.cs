﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Cava.Common.Security
{
    /// <summary>
    /// The ISecurityManager
    /// </summary>
    public interface ISecurityManager
    {
        /// <summary>
        /// Parsing token           
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        IPrincipal ParseToken(string token);

        /// <summary>
        /// Creaying token
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        string BuildToken(long? userID);

        /// <summary>
        /// The GeneratePassword
        /// </summary>
        /// <returns></returns>
        string GeneratePassword();
    }
}
