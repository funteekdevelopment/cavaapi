﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Cava.Common.Security
{
    /// <summary>
    /// The RequestPrincipal
    /// </summary>
    public class RequestPrincipal : IPrincipal
    {
        /// <summary>
        /// Initializing RequestPrincipal
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="userID"></param>
        /// <param name="date"></param>
        public RequestPrincipal(IIdentity identity, long? userID, DateTime date)
        {
            this.UserId = userID;
            this.Identity = identity;
            this.Date = date;
        }

        /// <summary>
        /// The UserId
        /// </summary>
        public long? UserId { get; private set; }

        /// <summary>
        /// The Identity
        /// </summary>
        public IIdentity Identity { get; private set; }

        /// <summary>
        /// The Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}
