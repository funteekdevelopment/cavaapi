﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cava.Common.Extensions
{
    public static class StringExtension
    {
        public static long? AsLong(this string value)
        {
            long result;
            if (long.TryParse(value, out result))
            {
                return result;
            }
            return null;
        }

        public static DateTime? AsDateTime(this string value)
        {
            DateTime result;
            if (DateTime.TryParse(value, out result))
            {
                return result;
            }
            return null;
        }
    }
}
