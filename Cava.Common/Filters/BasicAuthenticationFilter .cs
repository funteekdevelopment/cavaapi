﻿using Cava.Common.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Cava.Common.Filters
{
    /// <summary>
    /// The BasicAuthenticationFilter
    /// </summary>
    public class BasicAuthenticationFilter : AuthorizationFilterAttribute
    {
        /// <summary>
        /// The _securirtyManager
        /// </summary>
        private ISecurityManager _securirtyManager;

        /// <summary>
        /// The securityManager
        /// </summary>
        private ISecurityManager securityManager
        {
            get
            {
                if (this._securirtyManager == null) this._securirtyManager = new SecurityManager();

                return this._securirtyManager;
            }
        }

        /// <summary>
        /// The OnAuthorization
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var tokens = actionContext.Request.Headers.Where(e => e.Key == "token").Select(e => e.Value).FirstOrDefault();

            string token = null;

            if (tokens != null)
            {
                token = tokens.FirstOrDefault();
            }

            token = token ?? actionContext.Request.GetQueryNameValuePairs().Where(e => e.Key == "token").Select(e => e.Value).FirstOrDefault();

            HttpContext.Current.User = securityManager.ParseToken(token);

            base.OnAuthorization(actionContext);
        }
    }
}
